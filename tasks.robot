*** Settings ***
Documentation     GET ADDRESSES FROM DOPA
Library     RPA.Desktop.Windows
Library     RPA.Desktop
Library     RPA.Excel.Files
# Library     RPA.HTTP
Library     String
Library     Collections
Library     RequestsLibrary

*** Variables ***
${EXCEL_FILE}   cid.xlsx
${rowCount}     0
${runCount}     0
${theString}    

*** Keywords ***
Open The Testami
    Open Executable    TESTAMI.exe    Linkage Demo

Close The Testami
    Press keys    alt    f4

Click Read ID Card
    Mouse Click    name:readCardButton

Click LinkOffCode Textbox
    Mouse Click    name:LinkOffCodeTextbox

Click Send9080
    Mouse Click     name:send9080button

Click Open Pin Popup
    Mouse Click     name:openPinCodePopup

Enter PinCode
    ${pin}    Get Spaced String   1338 
    Send Keys To Input  ${pin}    with_enter=True

Click Send9081
    Mouse Click     name:send9081button

Click Send0101
    Mouse Click     name:send0101button

Click Send5000
    Mouse Click     name:send5000button

Delete Old CID
    Mouse Click     name:PidTargetTextbox
    Press keys      ctrl    a
    Press keys      delete

Get row count in the sheet
    [Arguments]      ${SHEET_NAME}
    ${sheet}=        Read Worksheet   ${SHEET_NAME}
    ${rows}=         Get Length  ${sheet}
    [Return]         ${rows}

Send notification to line group
    [Tags]    post
    Create Session    httpbin    https://notify-api.line.me/api/notify  verify=True
    &{data}=    Create Dictionary    message=โควต้าในการดึงข้อมูลถึงขีดจำกัด กรุณาเปลี่ยนบัตร by robot 
    &{headers}=    Create Dictionary    Authorization=Bearer qvSfMBTOxqIo81oZolhJvh9xacmmnJtauxt9q2gcUKO
    ${resp}=    Post Request    httpbin     /   params=${data}    headers=${headers}
    log to console   \nresp : ${resp.content}

Post house data to mflow
    [Arguments]    ${addressData} 
    
    [Tags]    post
    Create Session    httpbin    http://localhost:3001/enforcement-cmd-service/api/v1/create/address
    &{data}=    Create Dictionary    houseID=${addressData['houseID']}  houseNo=${addressData['houseNo']}
    # houseType=${houseType} houseTypeDesc=${houseTypeDesc} 
    #     ... alleyWayCode=${alleyWayCode} alleyWayDesc=${alleyWayDesc} alleyCode=${alleyCode} alleyDesc=${alleyDesc} 
    #     ... roadCode=${roadCode} roadDesc=${roadDesc} 
    #     ... subdistrictCode=${subdistrictCode} subdistrictDesc=${subdistrictDesc}
    #     ... districtCode=${districtCode}
    #     ... provinceCode=${provinceCode} provinceDesc=${provinceDesc}
    #     ... rcodeCode=${rcodeCode} rcodeDesc=${rcodeDesc}
    #     ... dateOfTerminate=${dateOfTerminate}
       
    &{headers}=    Create Dictionary    Authorization=Bearer qvSfMBTOxqIo81oZolhJvh9xacmmnJtauxt9q2gcUKO
    ${resp}=    Post Request    httpbin     /   params=${data}    headers=${headers}
    log to console    ${resp.content}
    [return]  ${resp.status_code}


*** Tasks ***
GET ADDRESS BOT
    # get auth from mflow service
    # ...

    # get cid list from mflow service
    [Tags]    get
    Create Session    httpbin    http://localhost:3001/enforcementservice/api/v1/address/docno/list
    &{headers}=    Create Dictionary    Authorization='Bearer qvSfMBTOxqIo81oZolhJvh9xacmmnJtauxt9q2gcUKO'
    ${resp}=    Get Request    httpbin     /       headers=${headers}
    ${jsondata}=    To Json    ${resp.content}
    ${countdocNo}=    Get length    ${jsondata['docnos']}
    # write to memory
    @{theCidList}=    Create List
    FOR    ${i}    IN RANGE    ${countdocNo}
        Append To List   ${theCidList}     ${jsondata['docnos'][${i}]['docNo']}
    END

    log to console   cid list : ${theCidList}

    # #PREPARE TEST AMI
    Open The Testami
    Click Read ID Card
    Click LinkOffCode Textbox
    Type Into   name:LinkOffCodeTextbox     01851
    Click Send9080
    Click Open Pin Popup
    Enter PinCode
    Click Send9081
    Type Into   name:OfficeCode     00023
    Type Into   name:VersionCode    01
    Type Into   name:ServiceCode    027

    FOR     ${cid}  IN      @{theCidList}
        log to console  start get data ${cid}
        Type Into   name:PidTargetTextbox   ${cid}
        Click Send5000
        &{adorableResult}   Get Text   name:adorableResult
        
        #DATA FROM DOPA (SPLIT FOR ADDRESS PART) 
        ${splitJSON} =   String.Split String    ${adorableResult.value}    JSON:
        ${addressData}=    To Json    ${splitJSON}[1]
        
        log to console  start Post Request
        ${reqStatus}=   Post house data to mflow    ${addressData}
        
        
        
        log to console  Post Response : ${reqStatus}

        Delete Old CID

        # #DATA FROM DOPA (SPLIT FOR STATUS PART) 
        # ${strAfterReturnStatus} =   String.Split String    ${adorableResult.value}    ReturnStatus:
        # ${returnStatus} =   String.Split String    ${strAfterReturnStatus[1]}    Data JSON:
        
        # # 90050 mean your card reach the maximum request per day
        # IF    ${returnStatus[0].strip()} == 90050
        #     log to console  hit the cap
        #     Send notification to line group
        # ELSE
        #     ${theString} =   Catenate    ${theString}    ${jsonData[1]}
        #     log to console  not reach the quato yet
        # END
        # Exit For Loop If    ${returnStatus[0].strip()} == 90050

        # Delete Old CID

        
        

        # prepare string for excel
        # IF    ${runCount} > 0    # runCount is use for String Concat    the 1st string don't need "," before [data]
        # ${theString} =   Catenate    ${theString}    ,
        # ${theString} =   Catenate    ${theString}    ${jsonData[1]}
        # log to console  \n${theString}
        # ELSE
        # ${theString} =   Catenate    ${theString}    ${jsonData[1]}
        # log to console  \n${theString}
        # END

        # ${runCount}=    Evaluate    ${runCount} + 1
    END 

    # ${addSquareBracket} =   Catenate  [ ${theString} ]
    # log to console   ${addSquareBracket}
    # ${convertedJSON}=    evaluate    json.loads('''${addSquareBracket}''')    json
    # Append Rows To Worksheet    ${convertedJSON}
    # Save Workbook

    # Close The Testami
